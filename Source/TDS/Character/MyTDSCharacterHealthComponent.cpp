// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

void UMyTDSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{

	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UMyTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	if (Shield > MaxShield)
	{
		Shield = MaxShield;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UMyTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime,false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UMyTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTDSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UMyTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > MaxShield)
	{
		Shield = MaxShield;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}

void UMyTDSCharacterHealthComponent::SetShieldVallue(float NewMaxShield)
{
	MaxShield = NewMaxShield;
	Shield = MaxShield;
}

float UMyTDSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UMyTDSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float Damage)
{
	OnShieldChange.Broadcast(NewShield, Damage);
}

void UMyTDSCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMyTDSCharacterHealthComponent, Shield);
}