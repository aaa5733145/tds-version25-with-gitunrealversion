// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDSGameMode.h"
#include "TDSPlayerController.h"
#include "Character/TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSGameMode::ATDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	// static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TDS/Blueprint/Character/TopDownCharacter"));
	// if (PlayerPawnBPClass.Class != NULL)
	// {
		 //DefaultPawnClass = PlayerPawnBPClass.Class;
	// }
	DefaultPawnClass = ATDSCharacter::StaticClass();
}

void ATDSGameMode::PlayerCharacterDead()
{

}
