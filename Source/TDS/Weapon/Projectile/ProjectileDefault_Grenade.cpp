// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"),DebugExplodeShow,TEXT("Draw Debug For Explode"),ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}


	if (ProjectileSetting.ExploseFX)
	{
		SpawnHitFXGranade_Multicast(ProjectileSetting.ExploseFX);
	}
	if (ProjectileSetting.ExploseSound)
	{
		SpawnHitSoundGranade_Multicast(ProjectileSetting.ExploseSound);
	}

	UE_LOG(LogTemp, Warning, TEXT("ExploseMaxDamage    = %f"), ProjectileSetting.ExploseMaxDamage);
	UE_LOG(LogTemp, Warning, TEXT("ExploseMinDamage    = %f"), ProjectileSetting.ExploseMinDamage);
	UE_LOG(LogTemp, Warning, TEXT("ProjectileMinRadiusDamage    = %f"), ProjectileSetting.ProjectileMinRadiusDamage);
	UE_LOG(LogTemp, Warning, TEXT("ProjectileMaxRadiusDamage    = %f"), ProjectileSetting.ProjectileMaxRadiusDamage);
	
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.FallOfDamage,
		UDamageType::StaticClass(),
		IgnoredActor,
		this,
		nullptr,
		ECollisionChannel::ECC_Visibility);

	TimerEnabled = false;
	UE_LOG(LogTemp, Warning, TEXT("TEXT RADIAL EXPLOSED"));
	this->Destroy(); 
}

void AProjectileDefault_Grenade::SpawnHitFXGranade_Multicast_Implementation(UParticleSystem* FXTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXTemplate, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnHitSoundGranade_Multicast_Implementation(USoundBase* HitSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
}
